# Expense Tracker

Expense Tracker is a simple Java application for tracking expenses.

## Features

- Add new expenses with amount, category, and date.
- Calculate monthly expenditure.
- Get expenses by category.
- Generate expense reports.

## Getting Started

### Prerequisites

- Java Development Kit (JDK) 8 or later
- Git

Usage

The application supports the following commands:

add <amount> <category> <date>: Add a new expense.
calculate <month> <year>: Calculate monthly expenditure.
get <category>: Get expenses by category.
help: Display available commands.
exit: Exit the program.